PREPARE - The Probe-response attack framework 
=============================================

This project implements the probe-response attack (PRA) using the effective attack logic
probosed by Bethencourt et al. (see Mapping Internet Sensors With Probe Response Attacks, 2005)
improved by further methodologies. The first one is made of a generic encoding scheme using checksums
to filter out non-probe events from attack reports. The second is a fingerprinting approach using feedback
on network level to cluster monitor nodes in the following attack iteration.
The attack framework is targeted against TraCINg and DShield and is extensible towards new CIDS (see Extension).
Furthermore this project contains a TraCINg installation extended to mitigate the threat
originated from PRAs and a simulation environment allowing to simulate attacks on TraCINg.

This project is made of the following parts:

* Probe-response attack framework (PREPARE) (see ./prepare)
* TraCINg, extended with Probe-Response attack mitigations (see ./tracing)
* Probe-Response attack simulation environment (see ./prepare)

## Requirements and installation ##
* The attack framework and simulation environment use raw sockets which implies root priviliges.
* Commands are assumed to be called from the base directory of the project.
* Do NOT use virtual interfaces not providing data link layer -> ZMap (pcap in particular) needs interfaces with data link access

### Linux based OS ###
* For Ubuntu the following command installs all needed packages. Depended on the OS version some version can deviate.
    : apt-get install libgmp3-dev libjson-c-dev libffi-dev npm nodejs-legacy gcc gengetopt byacc libpcap-dev libgmp3-dev mongodb flex cmake build-essential python-dev python3-dev python3-pip python-virtualenv arping

* The following should be installed for 32Bit-compatibility.
    : apt-get install lib32z1 lib32ncurses5 lib32bz2-1.0 libpcap0.8:i386 libcap-1*
* In the case of simulations the linux kernel module dummy has to be present (default on on Ubuntu)
* Configure linux to allow more memory usage for network purposes. Add the following to /etc/sysctl.conf:

    : net.core.rmem_default   = 268435456
    : net.core.wmem_max       = 2147483647
    : net.core.wmem_default   = 268435456
    : net.core.netdev_max_backlog = 10000000
    : Call: sysctl -p

* Disable sending RST to minimize traffic
    : iptables -A OUTPUT -p tcp -m tcp --tcp-flgas RST,RST RST,RST -j DROP
    : **Undo of the previous command:**
    : iptables -D OUTPUT -p tcp -m tcp --tcp-flags RST,RST RST,RST -j DROP

### Python 3.x ###
* Install and start virtualenv
    : virtualenv -p /usr/bin/python3.4 /root/.virtualenv
    : source /root/.virtualenv/bin/activate
* Install Python modules
    : pip install -r prepare/requirements.txt
* Warning: socketio_client needs a bug fix because of wrongly handled encodings (TraCINg sometimes uses non-UTF8 encodings which lets the lib crash)
    : cp prepare/bugfix_socketio/* /root/.virtualenv/lib/python3.4/site-packages/socketIO_client/


### ZMap ###
* The ZMap extension is based on Version v2.1.0-RC1
    : cd zmap
    : rm CMakeCache.txt
    : ./build_zmap.sh
    : ./update_ip_blacklist.sh
    : See README.md for more informations.

### TraCINg ###
* This is a bugfixed version of TraCINg (allowing ports 0 and 65535 in modules/postHandler.js)
* See TraCINg README for further information on installing/configuring.
* Some side notes:
    : Old modules of Node.js should be removed before installing: rm -rf node_modules
    : Some additional Node.js modules are used which should already be installed using the standard installation.
Otherwise they can be installed using: cd tracing && npm install chance collections event-stream fs

* Problems with MongoDB:
    : The Latency of Mongo-DB is too large on high traffic which is why
data insertion is commented out in postHandler.js (not needed)

## Usage ##
### General initiation ###
* Create virtual interface and initiate virtual environment (again: use root beacause of raw sockets) This implies virtualenv is placed ad /root/.virtualenv
    : . ./setup_evironment.sh startinterface

### Starting TraCINg ###
* cd tracing && node --max-new-space-size=4096 --max-old-space-size=2048 index.js (increase memory if needed)
* The PRA-mitigation can be activated/deactivated in tracing/modules/postHandler.js via praMitigation.setActive(false|true);
* The mitigation logic can be completely deactivated (see check_attack_status_ref in tracing/proberesponseMitigation.js)
* In order to introduce noise, this has to be activated (see noise_ref in tracing/modules/proberesponseMitigation.js)
Noise is read from a file called dshield_report.csv. The original DShield data has to be converted which can be done using
the script at tracing/modules/dshield_to_tracing.py.
* The file attack_stats.txt is updated with attack information.
* For a dedicated activation/deactivation of the mitigation logic see MITIGATION block comments in tracing/modules/proberesponseMitigation.js.

### Starting attack ###
* The attack framework has to be configured for the target system
* TraCINg
    : Set correkt tracing_domain in TracingReportFetcher in the attack framework (or create dedicated report fetcher)
* DShield
    : marker value bits=32, marker checksum bits=0 (when doing IP filtering)
    : use_source_ip_filter in report_fetcher.py
    : sleeptime in DShieldReportFetcher constructor
    : **optional: adjust parameters in main_attack.py:**
	    : ip_stage1 (main_attack.py)
	    : cidr_bits_stage1: Choose only a subnet to scan (main_attack.py)
	    : cores: amount of cores to be used (scanner_wrapper.py)
* Get gateway MAC via: arping [ip_gateway]
* Start framework, example calls:
    : python main_attack.py --help
    : python main_attack.py -i eth10 -m [mac_gateway] -r 10000 -b 32 -c 0
* A command line interface will appear which allows to control the framework. Type 'help' for further information on commands.

### Starting Simulation ###
* General initiation
* Adjust TraCINg domain in TracingReportFetcher to localhost
* Starting TraCINg
* cd prepare && python main_monitor_simulator.py -m 1000 [further arguments]
* Starting attack

## Extension ##
The attack framework can be extended for new CIDS by subclasing ReportFetcher in report_fetcher.py
and implementing before_scanning() and after_scanning(). The new class has to be placed
in the same module. The new report fetcher can be chosen by its class name when starting
the attack framework using the parameter report_fetcher_classname.
