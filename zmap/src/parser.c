#ifndef lint
static const char yysccsid[] = "@(#)yaccpar	1.9 (Berkeley) 02/21/93";
#endif

#define YYBYACC 1
#define YYMAJOR 1
#define YYMINOR 9
#define YYPATCH 20140101

#define YYEMPTY        (-1)
#define yyclearin      (yychar = YYEMPTY)
#define yyerrok        (yyerrflag = 0)
#define YYRECOVERING() (yyerrflag != 0)

#define YYPREFIX "yy"

#define YYPURE 0

#line 2 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
#include <stdio.h>
#include <string.h>
#include "expression.h"
#include "lexer.h"
#include "filter.h"

void yyerror(const char *str)
{
	fprintf(stderr,"Parse error: %s\n",str);
}
 
int yywrap()
{
	return 1;
}

extern node_t *zfilter;

#line 22 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
#ifdef YYSTYPE
#undef  YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
#endif
#ifndef YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
typedef union {
	int int_literal;
	char *string_literal;
	struct node_st *expr; 
} YYSTYPE;
#endif /* !YYSTYPE_IS_DECLARED */
#line 51 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.c"

/* compatibility with bison */
#ifdef YYPARSE_PARAM
/* compatibility with FreeBSD */
# ifdef YYPARSE_PARAM_TYPE
#  define YYPARSE_DECL() yyparse(YYPARSE_PARAM_TYPE YYPARSE_PARAM)
# else
#  define YYPARSE_DECL() yyparse(void *YYPARSE_PARAM)
# endif
#else
# define YYPARSE_DECL() yyparse(void)
#endif

/* Parameters sent to lex. */
#ifdef YYLEX_PARAM
# define YYLEX_DECL() yylex(void *YYLEX_PARAM)
# define YYLEX yylex(YYLEX_PARAM)
#else
# define YYLEX_DECL() yylex(void)
# define YYLEX yylex()
#endif

/* Parameters sent to yyerror. */
#ifndef YYERROR_DECL
#define YYERROR_DECL() yyerror(const char *s)
#endif
#ifndef YYERROR_CALL
#define YYERROR_CALL(msg) yyerror(msg)
#endif

extern int YYPARSE_DECL();

#define T_AND 257
#define T_OR 258
#define T_NUMBER 259
#define T_FIELD 260
#define T_NOT_EQ 261
#define T_GT_EQ 262
#define T_LT_EQ 263
#define YYERRCODE 256
static const short yylhs[] = {                           -1,
    0,    4,    4,    4,    4,    1,    1,    2,    2,    2,
    2,    2,    2,    3,    3,
};
static const short yylen[] = {                            2,
    1,    3,    3,    3,    1,    1,    1,    3,    3,    3,
    3,    3,    3,    3,    3,
};
static const short yydefred[] = {                         0,
    0,    0,    0,    5,    6,    7,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    4,   11,   15,   12,
    9,   10,    8,   14,   13,    3,    0,
};
static const short yydgoto[] = {                          3,
    4,    5,    6,    7,
};
static const short yysindex[] = {                       -40,
  -40,  -57,    0,    0,    0,    0, -250,  -39, -249, -245,
 -244, -243, -247, -242,  -40,  -40,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0, -248,
};
static const short yyrindex[] = {                         0,
    0,    0,    0,    0,    0,    0,   18,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    1,
};
static const short yygindex[] = {                         0,
    0,    0,    0,    5,
};
#define YYTABLESIZE 259
static const short yytable[] = {                          1,
    2,   17,   12,   13,   11,    8,   15,   16,   15,   18,
   19,   23,   24,   20,   21,   22,   25,    1,    0,   26,
   27,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    2,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    9,   10,   14,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,   15,   16,    2,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    2,
};
static const short yycheck[] = {                         40,
    0,   41,   60,   61,   62,    1,  257,  258,  257,  259,
  260,  259,  260,  259,  259,  259,  259,    0,   -1,   15,
   16,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   41,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,  261,  262,  263,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,  257,  258,  260,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  258,
};
#define YYFINAL 3
#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#define YYMAXTOKEN 263
#define YYTRANSLATE(a) ((a) > YYMAXTOKEN ? (YYMAXTOKEN + 1) : (a))
#if YYDEBUG
static const char *yyname[] = {

"end-of-file",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,"'('","')'",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"'<'","'='","'>'",0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"T_AND",
"T_OR","T_NUMBER","T_FIELD","T_NOT_EQ","T_GT_EQ","T_LT_EQ","illegal-symbol",
};
static const char *yyrule[] = {
"$accept : expression",
"expression : filter_expr",
"filter_expr : filter_expr T_OR filter_expr",
"filter_expr : filter_expr T_AND filter_expr",
"filter_expr : '(' filter_expr ')'",
"filter_expr : filter",
"filter : number_filter",
"filter : string_filter",
"number_filter : T_FIELD '=' T_NUMBER",
"number_filter : T_FIELD '>' T_NUMBER",
"number_filter : T_FIELD '<' T_NUMBER",
"number_filter : T_FIELD T_NOT_EQ T_NUMBER",
"number_filter : T_FIELD T_GT_EQ T_NUMBER",
"number_filter : T_FIELD T_LT_EQ T_NUMBER",
"string_filter : T_FIELD '=' T_FIELD",
"string_filter : T_FIELD T_NOT_EQ T_FIELD",

};
#endif

int      yydebug;
int      yynerrs;

int      yyerrflag;
int      yychar;
YYSTYPE  yyval;
YYSTYPE  yylval;

/* define the initial stack-sizes */
#ifdef YYSTACKSIZE
#undef YYMAXDEPTH
#define YYMAXDEPTH  YYSTACKSIZE
#else
#ifdef YYMAXDEPTH
#define YYSTACKSIZE YYMAXDEPTH
#else
#define YYSTACKSIZE 10000
#define YYMAXDEPTH  10000
#endif
#endif

#define YYINITSTACKSIZE 200

typedef struct {
    unsigned stacksize;
    short    *s_base;
    short    *s_mark;
    short    *s_last;
    YYSTYPE  *l_base;
    YYSTYPE  *l_mark;
} YYSTACKDATA;
/* variables for the parser stack */
static YYSTACKDATA yystack;
#line 143 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"


#line 252 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.c"

#if YYDEBUG
#include <stdio.h>		/* needed for printf */
#endif

#include <stdlib.h>	/* needed for malloc, etc */
#include <string.h>	/* needed for memset */

/* allocate initial stack or double stack size, up to YYMAXDEPTH */
static int yygrowstack(YYSTACKDATA *data)
{
    int i;
    unsigned newsize;
    short *newss;
    YYSTYPE *newvs;

    if ((newsize = data->stacksize) == 0)
        newsize = YYINITSTACKSIZE;
    else if (newsize >= YYMAXDEPTH)
        return -1;
    else if ((newsize *= 2) > YYMAXDEPTH)
        newsize = YYMAXDEPTH;

    i = (int) (data->s_mark - data->s_base);
    newss = (short *)realloc(data->s_base, newsize * sizeof(*newss));
    if (newss == 0)
        return -1;

    data->s_base = newss;
    data->s_mark = newss + i;

    newvs = (YYSTYPE *)realloc(data->l_base, newsize * sizeof(*newvs));
    if (newvs == 0)
        return -1;

    data->l_base = newvs;
    data->l_mark = newvs + i;

    data->stacksize = newsize;
    data->s_last = data->s_base + newsize - 1;
    return 0;
}

#if YYPURE || defined(YY_NO_LEAKS)
static void yyfreestack(YYSTACKDATA *data)
{
    free(data->s_base);
    free(data->l_base);
    memset(data, 0, sizeof(*data));
}
#else
#define yyfreestack(data) /* nothing */
#endif

#define YYABORT  goto yyabort
#define YYREJECT goto yyabort
#define YYACCEPT goto yyaccept
#define YYERROR  goto yyerrlab

int
YYPARSE_DECL()
{
    int yym, yyn, yystate;
#if YYDEBUG
    const char *yys;

    if ((yys = getenv("YYDEBUG")) != 0)
    {
        yyn = *yys;
        if (yyn >= '0' && yyn <= '9')
            yydebug = yyn - '0';
    }
#endif

    yynerrs = 0;
    yyerrflag = 0;
    yychar = YYEMPTY;
    yystate = 0;

#if YYPURE
    memset(&yystack, 0, sizeof(yystack));
#endif

    if (yystack.s_base == NULL && yygrowstack(&yystack)) goto yyoverflow;
    yystack.s_mark = yystack.s_base;
    yystack.l_mark = yystack.l_base;
    yystate = 0;
    *yystack.s_mark = 0;

yyloop:
    if ((yyn = yydefred[yystate]) != 0) goto yyreduce;
    if (yychar < 0)
    {
        if ((yychar = YYLEX) < 0) yychar = 0;
#if YYDEBUG
        if (yydebug)
        {
            yys = yyname[YYTRANSLATE(yychar)];
            printf("%sdebug: state %d, reading %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
    }
    if ((yyn = yysindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: state %d, shifting to state %d\n",
                    YYPREFIX, yystate, yytable[yyn]);
#endif
        if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
        {
            goto yyoverflow;
        }
        yystate = yytable[yyn];
        *++yystack.s_mark = yytable[yyn];
        *++yystack.l_mark = yylval;
        yychar = YYEMPTY;
        if (yyerrflag > 0)  --yyerrflag;
        goto yyloop;
    }
    if ((yyn = yyrindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
        yyn = yytable[yyn];
        goto yyreduce;
    }
    if (yyerrflag) goto yyinrecovery;

    yyerror("syntax error");

    goto yyerrlab;

yyerrlab:
    ++yynerrs;

yyinrecovery:
    if (yyerrflag < 3)
    {
        yyerrflag = 3;
        for (;;)
        {
            if ((yyn = yysindex[*yystack.s_mark]) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: state %d, error recovery shifting\
 to state %d\n", YYPREFIX, *yystack.s_mark, yytable[yyn]);
#endif
                if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
                {
                    goto yyoverflow;
                }
                yystate = yytable[yyn];
                *++yystack.s_mark = yytable[yyn];
                *++yystack.l_mark = yylval;
                goto yyloop;
            }
            else
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: error recovery discarding state %d\n",
                            YYPREFIX, *yystack.s_mark);
#endif
                if (yystack.s_mark <= yystack.s_base) goto yyabort;
                --yystack.s_mark;
                --yystack.l_mark;
            }
        }
    }
    else
    {
        if (yychar == 0) goto yyabort;
#if YYDEBUG
        if (yydebug)
        {
            yys = yyname[YYTRANSLATE(yychar)];
            printf("%sdebug: state %d, error recovery discards token %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
        yychar = YYEMPTY;
        goto yyloop;
    }

yyreduce:
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: state %d, reducing by rule %d (%s)\n",
                YYPREFIX, yystate, yyn, yyrule[yyn]);
#endif
    yym = yylen[yyn];
    if (yym)
        yyval = yystack.l_mark[1-yym];
    else
        memset(&yyval, 0, sizeof yyval);
    switch (yyn)
    {
case 1:
#line 45 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
		zfilter = yystack.l_mark[0].expr;
	}
break;
case 2:
#line 52 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
			yyval.expr = make_op_node(OR);
			yyval.expr->left_child = yystack.l_mark[-2].expr;
			yyval.expr->right_child = yystack.l_mark[0].expr;
		}
break;
case 3:
#line 58 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
			yyval.expr = make_op_node(AND);
			yyval.expr->left_child = yystack.l_mark[-2].expr;
			yyval.expr->right_child = yystack.l_mark[0].expr;
		}
break;
case 4:
#line 64 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
			yyval.expr = yystack.l_mark[-1].expr;
		}
break;
case 5:
#line 68 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
			yyval.expr = yystack.l_mark[0].expr;
		}
break;
case 6:
#line 74 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
			yyval.expr = yystack.l_mark[0].expr;
		}
break;
case 7:
#line 78 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
			yyval.expr = yystack.l_mark[0].expr;
		}
break;
case 8:
#line 84 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
			yyval.expr = make_op_node(EQ);
			yyval.expr->left_child = make_field_node(yystack.l_mark[-2].string_literal);
			yyval.expr->right_child = make_int_node(yystack.l_mark[0].int_literal);
		}
break;
case 9:
#line 91 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
			yyval.expr = make_op_node(GT);
			yyval.expr->left_child = make_field_node(yystack.l_mark[-2].string_literal);
			yyval.expr->right_child = make_int_node(yystack.l_mark[0].int_literal);
		}
break;
case 10:
#line 98 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
			yyval.expr = make_op_node(LT);
			yyval.expr->left_child = make_field_node(yystack.l_mark[-2].string_literal);
			yyval.expr->right_child = make_int_node(yystack.l_mark[0].int_literal);
		}
break;
case 11:
#line 105 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
			yyval.expr = make_op_node(NEQ);
			yyval.expr->left_child = make_field_node(yystack.l_mark[-2].string_literal);
			yyval.expr->right_child = make_int_node(yystack.l_mark[0].int_literal);
		}
break;
case 12:
#line 112 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
			yyval.expr = make_op_node(GT_EQ);
			yyval.expr->left_child = make_field_node(yystack.l_mark[-2].string_literal);
			yyval.expr->right_child = make_int_node(yystack.l_mark[0].int_literal);
		}
break;
case 13:
#line 119 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
			yyval.expr = make_op_node(LT_EQ);
			yyval.expr->left_child = make_field_node(yystack.l_mark[-2].string_literal);
			yyval.expr->right_child = make_int_node(yystack.l_mark[0].int_literal);
		}
break;
case 14:
#line 128 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
			yyval.expr = make_op_node(EQ);
			yyval.expr->left_child = make_field_node(yystack.l_mark[-2].string_literal);
			yyval.expr->right_child = make_string_node(yystack.l_mark[0].string_literal);
		}
break;
case 15:
#line 135 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.y"
	{
			yyval.expr = make_op_node(NEQ);
			yyval.expr->left_child = make_field_node(yystack.l_mark[-2].string_literal);
			yyval.expr->right_child = make_string_node(yystack.l_mark[0].string_literal);
		}
break;
#line 564 "/home/mike/folder/studium_ms/semester_5/master_thesis/repo_prepare/zmap/src/parser.c"
    }
    yystack.s_mark -= yym;
    yystate = *yystack.s_mark;
    yystack.l_mark -= yym;
    yym = yylhs[yyn];
    if (yystate == 0 && yym == 0)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: after reduction, shifting from state 0 to\
 state %d\n", YYPREFIX, YYFINAL);
#endif
        yystate = YYFINAL;
        *++yystack.s_mark = YYFINAL;
        *++yystack.l_mark = yyval;
        if (yychar < 0)
        {
            if ((yychar = YYLEX) < 0) yychar = 0;
#if YYDEBUG
            if (yydebug)
            {
                yys = yyname[YYTRANSLATE(yychar)];
                printf("%sdebug: state %d, reading %d (%s)\n",
                        YYPREFIX, YYFINAL, yychar, yys);
            }
#endif
        }
        if (yychar == 0) goto yyaccept;
        goto yyloop;
    }
    if ((yyn = yygindex[yym]) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn];
    else
        yystate = yydgoto[yym];
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: after reduction, shifting from state %d \
to state %d\n", YYPREFIX, *yystack.s_mark, yystate);
#endif
    if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
    {
        goto yyoverflow;
    }
    *++yystack.s_mark = (short) yystate;
    *++yystack.l_mark = yyval;
    goto yyloop;

yyoverflow:
    yyerror("yacc stack overflow");

yyabort:
    yyfreestack(&yystack);
    return (1);

yyaccept:
    yyfreestack(&yystack);
    return (0);
}
