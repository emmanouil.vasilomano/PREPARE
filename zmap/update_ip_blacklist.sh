#!/bin/bash
# Updates the current blacklist.conf with a full bogon list from
# http://www.team-cymru.org/Services/Bogons/fullbogons-ipv4.txt
CURRENT_DIR=$(pwd)
FILENAME_BLACKLIST="$CURRENT_DIR/blacklist.conf"
echo "saving full bogon to: $FILENAME_BLACKLIST"
wget http://www.team-cymru.org/Services/Bogons/fullbogons-ipv4.txt -O $FILENAME_BLACKLIST
# RFC0919: Limited Broadcast
echo "255.255.255.255/32" >> $FILENAME_BLACKLIST
# Add IP range of TUD
echo -e "# TUD network\n130.83.0.0/16" >> $FILENAME_BLACKLIST
# Add Strato network
echo -e "# Strato\n81.169.128.0/17" >> $FILENAME_BLACKLIST
echo -e "# Strato\n81.169.128.0/17\n85.214.0.0/16" >> $FILENAME_BLACKLIST
