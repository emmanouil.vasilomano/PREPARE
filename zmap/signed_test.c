#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
/*
0x00000000	0xFFFFFFFF
0xFFFFFFFF	0xFFFFFFFF
0xFFFFFFFE	0xFFFEFFFE
0x00020304	0x03080306
*/

//uint32_t fletcher32(uint16_t const *data, size_t words)
uint32_t fletcher32(unsigned short const *data, size_t words)
{
	printf("checksum calculation...\n");
	// 1 word = 2 Bytes
        uint32_t sum1 = 0xffff, sum2 = 0xffff;

        while (words) {
                unsigned tlen = words > 359 ? 359 : words;
                words -= tlen;
                do {
                        //sum2 += sum1 += *data++;
			printf("adding NBO: 0x%.4" PRIX32 "\n", htons(*data));
			printf("adding HBO: 0x%.4" PRIX32 "\n", *data);
			//sum1 += *data;
			sum1 += htons(*data);
                        sum2 += sum1;
                        data += 1;
			//printf("---\n");
                } while (--tlen);
                sum1 = (sum1 & 0xffff) + (sum1 >> 16);
                sum2 = (sum2 & 0xffff) + (sum2 >> 16);
        }
        /* Second reduction step to reduce sums to 16 bits */
        sum1 = (sum1 & 0xffff) + (sum1 >> 16);
        sum2 = (sum2 & 0xffff) + (sum2 >> 16);
        return sum2 << 16 | sum1;
}

int main() {
	uint32_t ui, sum;

	ui = htonl((uint32_t)0x00020304);
	ui = htonl((uint32_t)0xFFFFFF00);
	ui = htonl((uint32_t)0x00000000);
	//ui = htonl((uint32_t)0x00000002);
	ui = htonl((uint32_t)0x02000000);
	//ui = htonl((uint32_t)0x00000000);
	//ui = htonl((uint32_t)0xFFFFFFFE);
	unsigned short const *data = (unsigned short*)&ui;
	unsigned char const *data2 = (char*)&ui;

	printf("1st half: 0x%.4" PRIX32 "\n", *data);
	printf("2nd half: 0x%.4" PRIX32 "\n", *(data+1));
	printf("0x%.4" PRIX32 "\n", *data2);
	printf("0x%.4" PRIX32 "\n", *(data2+1));
	printf("0x%.4" PRIX32 "\n", *(data2+2));
	printf("0x%.4" PRIX32 "\n", *(data2+3));

	sum = fletcher32(data, 2);

	printf("0x%.8" PRIX32 "\n", sum);
}
