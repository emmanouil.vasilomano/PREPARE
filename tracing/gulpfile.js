var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-minify-css');

var frontend_path = './frontend/';
var extern_path = frontend_path + 'extern/';

gulp.task('build-extern-js', function(){
	var extern = [
		'jquery/jquery.min.js',
		'jquery/jquery-ui.js',
		'jquery/jquery.multiselect.min.js',
		'jquery/jquery.ui.jrange.js',
		'bootstrap/bootstrap.min.js',
		'highcharts/highcharts.js',
		'jvectormap/jquery-jvectormap.min.js',
		'jvectormap/jquery-jvectormap-world-mill-en.js',
		'leaflet/leaflet.js',
		'globe/three.min.js',
		'globe/globe.min.js',
		'datatables/jquery.dataTables.min.js',
		'datatables/DT_bootstrap.js',
		'jquery/jquery.ba-throttle-debounce.min.js'
	];

	return gulp.src(extern.map(function(f){
			return extern_path + f;
	})).pipe(concat('all-extern.js'))
		.pipe(uglify())
		.pipe(gulp.dest(frontend_path + 'build/'));
});

gulp.task('build-internal-js', function(){
	var intern = [
		'countryName.js',
		'map.js',
		'streetmap.js',
		'statistics.js',
		'controller.js',
		'world.js',
		'dbWindow.js',
		'help.js',
		'menu.js',
		'filter.js',
		'main.js'
	];

	return gulp.src(intern.map(function(f){
		return frontend_path + f;
	})).pipe(concat('all-intern.js'))
		.pipe(uglify())
		.pipe(gulp.dest(frontend_path + 'build/'));
});

gulp.task('build-external-css', function(){
	var extern = [
		'jquery/jquery-ui.css',
		'jquery/jquery.multiselect.css',
		'jquery/ui.jrange.css',
		'bootstrap/bootstrap.min.css',
		'bootstrap/bootstrap-responsive.min.css',
		'jvectormap/jquery-jvectormap.css',
		'leaflet/leaflet.css',
		'globe/globe.css',
		'datatables/jquery.dataTables.css',
		'datatables/DT_bootstrap.css'
	];

	return gulp.src(extern.map(function(f){
		return extern_path + f;
	})).pipe(concat('all-extern.css'))
		.pipe(minifyCSS())
		.pipe(gulp.dest(frontend_path + 'build/'));
});

gulp.task('build-internal-css', function(){
	return gulp.src(frontend_path + 'main.css')
		.pipe(minifyCSS())
		.pipe(gulp.dest(frontend_path + 'build/'));
});

gulp.task('default', ['build-extern-js', 'build-internal-js', 'build-external-css', 'build-internal-css']);