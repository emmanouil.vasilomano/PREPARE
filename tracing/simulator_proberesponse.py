# This is a customized version of the TraCINg attack simulator needed for
# simulating probe response attacks.
#
# Requirements:
# - Python 3.x
# - requests
#
# TraCINg-Server - Gathering and visualizing cyber incidents on the world
#
# Copyright 2013 Matthias Gazzari, Annemarie Mattmann, André Wolski
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import requests
import random
import json
import time
import sys
import http.client
import time
# In order to enable server certificate check activate this imports
#import ssl

# disable warnings as we are simulating locally
logging.captureWarnings(True)

logger = logging.getLogger("probe_response_attack")
# based on http://stackoverflow.com/questions/10218486/set-nested-dict-value-and-create-intermediate-keys
#from collections import defaultdict
#recursivedict = lambda: defaultdict(recursivedict)

# constants and defaults
sensor_name_base = "Simulator"
sensorType = "Honeypot"
#url = "https://localhost:9999"
url = "https://localhost:443"
cert = ("ssl/simulator/simulator_cert.pem", "ssl/simulator/simulator_key.pem")

"""
headers = {"Content-type": "application/x-www-form-urlencoded",
			"Accept": "text/plain"}
"""
#conn = http.client.HTTPSConnection("localhost", 443)
request_session = requests.Session()

incidentTypes = {
	0: "Unknown",
	10: "Transport Layer",
	11: "Portscan",
	20: "Shellcode Injection",
	30: "SQL",
	31: "MySQL",
	32: "MS SQL",
	40: "SMB",
	50: "VoIP",
	60: "Invalid"		# invalid test case
}


def get_full_entry(ip_src=None, port_src=None, ip_dst=None, port_dst=None, sensor_name=None):
	"""
	Return an entry using every fields possible.
	"""
	if sensor_name is None:
		sensor_name = sensor_name_base + ip_dst
	return {
		"sensor": {
			"name": sensor_name,
			"type": sensorType,
		},
		"src": {
			"ip": ip_src,
			"port": port_src,
		},
		"dst": {
			"ip": ip_dst,
			"port": port_dst,
		},
		"type": 11,
		"log": "Random Testlog",
		"md5sum": "0x0123456789",
		"date": int(time.time()),
	}


def send_accident(ip_src, port_src, ip_dst, port_dst, url=url, cert=None, verify=False):
	"""
	Send an accident to the server.
	"""
	entry = get_full_entry(ip_src=ip_src, port_src=port_src, ip_dst=ip_dst, port_dst=port_dst)
	#logger.warning("sending entry: %r" % entry)

	try:
		#s.headers.update(headers)
		request_session.post(url=url, data=json.dumps(entry), verify=False)
		#conn.request("POST", "/", body=json.dumps(entry))
		#response = requests.post(url, cert=cert, verify=verify, data=json.dumps(entry))
	except Exception as e:
		logger.warning("could not send request: %r" % e)

rr = random.randrange

def attack_single_source_single_dest(amount):
	for x in range(amount):
		ip_src = "12.34.56.78"
		ip_dst = "56.34.56.79"

		send_accident(
			ip_src=ip_src, port_src=65535,
			ip_dst=ip_dst, port_dst=65535)

def attack_single_source_multiple_dest(amount):
	for x in range(amount):
		ip_src = "12.34.56.78"
		ip_dst = "%d.%d.%d.%d" % (rr(0, 255), rr(0, 255), rr(0, 255), rr(0, 255))

		send_accident(
			ip_src=ip_src, port_src=65535,
			ip_dst=ip_dst, port_dst=65535)

def attack_multiple_source_multiple_dest(amount):
	for x in range(amount):
		ip_src = "%d.%d.%d.%d" % (rr(0, 255), rr(0, 255), rr(0, 255), rr(0, 255))
		ip_dst = "%d.%d.%d.%d" % (rr(0, 255), rr(0, 255), rr(0, 255), rr(0, 255))

		send_accident(
			ip_src=ip_src, port_src=65535,
			ip_dst=ip_dst, port_dst=65535)

def attack_multiple_source_single_dest(amount):
	for x in range(amount):
		ip_src = "%d.%d.%d.%d" % (rr(0, 255), rr(0, 255), rr(0, 255), rr(0, 255))
		ip_dst = "12.34.56.78"

		send_accident(
			ip_src=ip_src, port_src=65535,
			ip_dst=ip_dst, port_dst=65535)

def attack_single_source_multiple_destport(amount):
	for x in range(amount):
		ip_src = "12.34.56.78"
		ip_dst = "12.34.56.79"

		send_accident(
			ip_src=ip_src, port_src=65535,
			ip_dst=ip_dst, port_dst=rr(0, 65535))

if __name__ == '__main__':
	if len(sys.argv) <= 1:
		send_accident( ip_src="123.45.67.89",
			port_src=65535,
			ip_dst="23.45.67.89",
			port_dst=65535)
		send_accident( ip_src="123.45.67.89",
			port_src=65535,
			ip_dst="23.45.67.89",
			port_dst=65535)
	else:
		amount = int(sys.argv[1])
		#attack_single_source_single_dest(amount)
		#attack_single_source_multiple_dest(amount)
		#attack_multiple_source_multiple_dest(amount)
		#attack_multiple_source_single_dest(amount)
		attack_single_source_multiple_destport(amount)
