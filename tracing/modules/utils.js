var StringUtils = {
	startsWith: function(str, start){
		return str.lastIndexOf(start) === 0;
	}
};

module.exports = {
	startsWith: StringUtils.startsWith
};