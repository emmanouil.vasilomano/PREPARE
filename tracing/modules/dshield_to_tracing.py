import sys
import csv
import random
import re
from collections import defaultdict

if len(sys.argv) < 2:
	print("usage: python %s dshield_stats.csv outfile.csv" % sys.argv[0])
	sys.exit(1)

csv_filename_in = sys.argv[1]
csv_fd_in = open(csv_filename_in, newline='')
csv_reader = csv.reader(csv_fd_in, delimiter='	')

csv_filename_out = sys.argv[2]
csv_fd_out = open(csv_filename_out, "w")

print("converting DShield data from %s to %s" % (csv_filename_in, csv_filename_out))
cnt = 0

for val in csv_reader:
	if len(val) < 9:
		continue
	if cnt % 1000000 == 0:
		print("got %d so far" % cnt)
	cnt += 1

	csv_fd_out.write("%s\t%s\t%s\t%s\t%s\t%s\n" %
		(val[3], val[4], val[5], val[6], val[7], val[8]))


csv_fd_in.close()
csv_fd_out.close()
