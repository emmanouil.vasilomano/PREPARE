IFACE="eth10"
echo "deactivating previous environment"
deactivate

#rm /var/lib/mongodb/mongod.lock
#mongod --repair
#ervice mongodb start


if [ "$1" == "startinterface" ]; then
echo "configuring interface"
modprobe dummy 1>&/dev/null
ip link set name $IFACE dev dummy0 1>&/dev/null
ip link show $IFACE
ifconfig $IFACE txqueuelen 10000
ifconfig $IFACE 1.1.1.1
ifconfig $IFACE hw ether be:af:8b:42:ca:d3
ifconfig $IFACE up


if [ -n "$(grep -i "Ubuntu" <<< $(uname -a))" ]; then
echo "restarting mongodb for Ubuntu"
service mongodb stop
rm /var/lib/mongodb/tracing.*
rm /var/lib/mongodb/local.*
rm /var/lib/mongodb/mongod.lock
rm -rf /var/lib/mongodb/tracing
#mongod --repair --dbpath /var/lib/mongodb/
service mongodb start
else
echo "restarting mongodb for non-Ubuntu"
# clear mongo db databases, see /etc/mongodb.conf
# alternativelly call: mongod --repair --dbpath /var/lib/mongodb/
/etc/init.d/mongodb stop 1>&/dev/null

rm /var/lib/mongodb/tracing.*
rm /var/lib/mongodb/local.*
rm /var/lib/mongodb/mongod.lock

#mongod --repair --dbpath /var/lib/mongodb/
/etc/init.d/mongodb start
/etc/init.d/mongodb status
fi
fi

# Prepare isolated environment
#virtualenv -p /usr/bin/python2.6 <path/to/new/virtualenv/>
#virtualenv $VIRTUAL_ENV

echo "setting up virtual env for python"
# Info: call via: srouce [script]
# Setup virtual environment
echo "setting VIRTUAL_ENV=$HOME/.virtualenv"
VIRTUAL_ENV=$HOME/.virtualenv


# Activate isolated environment
echo "activating isolated environment"
source $VIRTUAL_ENV/bin/activate
